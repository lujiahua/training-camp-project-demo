package hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lv-success
 * @since 2018-05-04
 */
@ApiModel(value = "学生实体")
public class Student extends Model<Student> {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    @ApiModelProperty("性别")
    private String sex;
    private Integer age;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", sex=" + sex + ", age=" + age + "]";
	}
	@Override
	protected Serializable pkVal() {
		return this.id;
	}


    
}
